"use client";

import Image from 'next/image';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { signIn, signOut, useSession, getProviders } from "next-auth/react"

const Nav = () => {
  const isUserLoggedIn = true;
  const [providers, setProviders] = useState(null)
  const [toggleDropdown, setToggleDropdown] = useState(false)

  useEffect(() => {
    const setProviders = async () => {
      const res = await getProviders()
      setProviders(res)
    }

    setProviders();
  }, [])

  return (
    <nav className='flex-between w-full mb-16 pt-3'>
      <Link href="/" className='flex gap-2 flex-center' >
        <Image src="/assets/images/logo.svg"
          alt='Promptopia logo'
          width={30}
          height={30}
          className='object-contain'
        />
        <p>Promptopia</p>
      </Link>

      {/* Desktop navigation */}

      <div className='md:flex hidden '>
        {isUserLoggedIn ? (
          <div className='flex gap-3 md:gap-5'>
            <Link href="/create-prompt" className='black_btn'>
              Create Post
            </Link>
            <button type='button' className='outline_btn' onClick={signOut} >Sign Out</button>
            <Link href='/profile' >
              <Image
                src="/assets/images/logo.svg"
                width={37}
                height={37}
                alt='Profile'
                className='rounded-full'
              />
            </Link>
          </div>
        ) : (
          <>
            {providers && object.values(providers).map((provider) => {
              return <button key={provider.name} type='button' onClick={() => signIn(provider.id)}>

              </button>
            })}
          </>
        )}

      </div>

      {/* mobile navigation  */}
      <div className='sm:hidden flex relative'>
        {isUserLoggedIn ? (
          <div className='flex'>
            <Image
                src="/assets/images/logo.svg"
                width={37}
                height={37}
                alt='Profile'
                className='rounded-full'
                onClick={()=>{setToggleDropdown((prev)=>!prev)}}
              />
              { toggleDropdown && (
                <div className="dropdown">
                  <Link href="/profile"
                  className='dropdown_link'
                  onClick={()=>{setToggleDropdown(false)}}>
                    My Profile
                  </Link>
                  <Link href="/create-prompt"
                  className='dropdown_link'
                  onClick={()=>{setToggleDropdown(false)}}>
                    Create Prompt
                  </Link>
                  <button type='button' 
                  onClick={()=>{
                    setToggleDropdown(false)
                    signOut();
                  }}
                  className='black_btn mt-5 w-full'>
                    Sign Out
                  </button>
                </div>
              )}
          </div>
        ) : (
          <>
            {providers && object.values(providers).map((provider) => {
              return <button key={provider.name} type='button' onClick={() => signIn(provider.id)}>

              </button>
            })}
          </>
        )}

      </div>
    </nav>
  )
}

export default Nav